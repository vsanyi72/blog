/**
 * 
 */
package vs.blog.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import vs.blog.model.BlogItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author sanyi Ez az oszt�ly gondoskodik a blog.txt file beolvas�sr�l
 *         �s ki�r�s�r�l
 */
public class Reader {

	private File file;
	private String focim;

	public Reader(String path) {
		super();
		this.file = new File(path);
	}

	public SajatArrayList<BlogItem> readBlogItem() throws ParseException {
		SajatArrayList<BlogItem> blogItem = new SajatArrayList<>();
		String sor;
		try (BufferedReader br = new BufferedReader(new FileReader(this.file))) {
			focim = br.readLine();
			// System.out.println("A Blog c�me: "+focim);
			while ((sor = br.readLine()) != null) {
				// sor splitel�ssel val� sz�tszed�se
				String sorId = sor.split(";")[0];
				int id = Integer.parseInt(sorId);
				String blogCim = sor.split(";")[1];
				String blogDatum = sor.split(";")[2];
				String blogSzoveg = sor.split(";")[3];
				boolean blogSzerkesztve = Boolean.parseBoolean(sor.split(";")[4]);

				// szemely p�ld�nyos�t�sa
				BlogItem bi = new BlogItem(id, blogCim, blogDatum, blogSzoveg, blogSzerkesztve);

				// listhez ad�s
				blogItem.add(bi);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return blogItem;
	}

	public String getFocim() {
		return focim;
	}

	public void setFocim(String focim) {
		this.focim = focim;
	}
}
