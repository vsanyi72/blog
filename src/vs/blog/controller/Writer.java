package vs.blog.controller;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import vs.blog.model.BlogItem;

public class Writer {

	private File file;

	public Writer(String path) {
		super();
		this.file = new File(path);

	}

	public void writeBlog(List<BlogItem> blogItem, String focim) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(this.file));
		String blogItemToWrite="";
		for (int i = 0; i < blogItem.size(); i++)  {
			blogItemToWrite+=blogItem.get(i).getId();
			blogItemToWrite+=";";
			blogItemToWrite+=blogItem.get(i).getBlogCim();
			blogItemToWrite+=";";
			blogItemToWrite+=blogItem.get(i).getBlogDatum();
			blogItemToWrite+=";";
			blogItemToWrite+=blogItem.get(i).getBlogSzoveg();
			blogItemToWrite+=";";
			blogItemToWrite+=blogItem.get(i).getBlogSzerkesztve();
			blogItemToWrite+="\n";
		}
		bw.write(focim + "\n" + blogItemToWrite);
		bw.close();
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}
