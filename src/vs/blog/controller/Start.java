package vs.blog.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import vs.blog.model.BlogItem;
import vs.blog.view.Beker;
import vs.blog.view.View;

public class Start {

	public static void main(String[] args) {

		View megjelenit = new View();

		boolean kilep = false;
		while (!kilep) {

			int menu = megjelenit.menu();
			String path = "d://work/java/Blog/blog.txt"; 
			switch (menu) {
			case 1:
				ujBejegyzes(path);
				break;
			case 2:
				bejegyzesModositas(path);
				break;
			case 3:
				bejegyzesTorles(path);
				break;
			case 4:
				blogLista(path);
				break;
			case 5:
				System.exit(0);
			default:
				System.out.println("Nem megfelelõ menüpont");
				break;
			}
		}

	}

	private static void ujBejegyzes(String path) {

		Reader reader = new Reader(path);
		String focim = "";
		int newId = 0;
		SajatArrayList<BlogItem> blog = null;

		try {
			blog = reader.readBlogItem();
			focim = reader.getFocim();
			newId = blog.get(blog.size() - 1).getId() + 1;
			System.out.println("azonosító: " + newId);

		} catch (ParseException e) {

			e.printStackTrace();
		}

		Beker beker = new Beker();
		String blogNeve = beker.blogNeve();
		String blogSzoveg = beker.blogSzoveg();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String strDate = sdf.format(date);
		Writer writer = new Writer(path);
		BlogItem blogItem = new BlogItem(newId, blogNeve, strDate, blogSzoveg, false);
		blog.add(blogItem);
		try {
			writer.writeBlog(blog, focim);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void bejegyzesModositas(String path) {
		Reader reader = new Reader(path);
		SajatArrayList<BlogItem> blog = null;
		Beker beker = new Beker();
		int blogId = beker.blogId();
		int idIndexModBlog = 0;
		boolean vanIlyenId = false;
		try {
			blog = reader.readBlogItem();
			for (int i = 0; i < blog.size(); i++) {
				if (blog.get(i).getId() == blogId) {
					idIndexModBlog = i;
					vanIlyenId = true;
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (vanIlyenId == true) {
//			String focim = "";
			String blogNeve = beker.blogNeve();
			String blogSzoveg = beker.blogSzoveg();
			System.out.println();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String strDate = sdf.format(date);

			Writer writer = new Writer(path);
			BlogItem blogItem = new BlogItem(blogId, blogNeve, strDate, blogSzoveg, true);
			blog.remove(idIndexModBlog);
			blog.add(idIndexModBlog, blogItem);
			String focim = reader.getFocim();
			try {
				writer.writeBlog(blog, focim);
			} catch (IOException e) {

				e.printStackTrace();
			}
		} else {
			System.out.println("Nincs ilyen blog bejegyzés.");
			System.out.println();
		}

	}

	private static void bejegyzesTorles(String path) {
		Reader reader = new Reader(path);
		SajatArrayList<BlogItem> blog;
		Beker beker = new Beker();
		int blogId = beker.blogId();
		int idTorolni = 0;
		boolean vanIlyenId = false;
		try {
			blog = reader.readBlogItem();
			for (int i = 0; i < blog.size(); i++) {
				if (blog.get(i).getId() == blogId) {
					idTorolni = i;
					vanIlyenId = true;
				}
			}
			if (vanIlyenId == true) {
				blog.remove(idTorolni);
				String focim = reader.getFocim();
				Writer writer = new Writer(path);
				try {
					writer.writeBlog(blog, focim);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				System.out.println("Nincs ilyen blog bejegyzés.");
				System.out.println();
			}

		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	private static void blogLista(String path) {
		Reader reader = new Reader(path);
		List<BlogItem> blog;
		try {
			blog = reader.readBlogItem();
			System.out.println("A blog címe : " + reader.getFocim());
			System.out.println(blog.toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
