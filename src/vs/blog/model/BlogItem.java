/**
 * 
 */
package vs.blog.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author sanyi Egy teljes blog bejegyz�s
 * 
 */
public class BlogItem {
	public BlogItem() {
		super();
	}

	private int id;
	private String blogCim;
	private String blogDatum;
	private String blogSzoveg;
	private boolean blogSzerkesztve;

	public BlogItem(int id, String blogCim, String blogDatum, String blogSzoveg, boolean blogSzerkesztve) {
		super();
		this.id = id;
		this.blogCim = blogCim;
		this.blogDatum = blogDatum;
		this.blogSzoveg = blogSzoveg;
		this.blogSzerkesztve = blogSzerkesztve;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBlogCim() {
		return blogCim;
	}

	public void setBlogCim(String blogCim) {
		this.blogCim = blogCim;
	}

	public String getBlogDatum() {
		return blogDatum;
	}

	public void setBlogDatum(String blogDatum) {
		this.blogDatum = blogDatum;
	}

	public String getBlogSzoveg() {
		return blogSzoveg;
	}

	public void setBlogSzoveg(String blogSzoveg) {
		this.blogSzoveg = blogSzoveg;
	}

	public boolean getBlogSzerkesztve() {
		return blogSzerkesztve;
	}

	public void setBlogSzerkesztve(boolean blogSzerkesztve) {
		this.blogSzerkesztve = blogSzerkesztve;
	}

	@Override
	public String toString() {
		return new StringBuilder().append(this.getBlogDatum()).append(" (" + this.getId() + ")")
				.append("  -  " + this.getBlogCim()).append("\n      ").append(this.getBlogSzoveg()).append("\n").toString();

	}

	}
